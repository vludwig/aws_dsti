# AWS DSTI Project1

This is a documentation page for the AWS Architecture class of DSTI 2018.


## Desired Architecture
The network should contain a private server which communicates with the web via 
a public server.
![Architecture](pics/aws_architecture.png)

## VPC & Subnets
First we setup a VPC named MyVPC with two subnets, one private and one public.

![VPC](pics/vpc2.png)

![Subnets](pics/subnets.png)

## IGW 
We setup a Internet-Gateway and attach it to our vpc.

![IGW](pics/igw.png)

## NAT Gateway
The NAT Gateway will redirect all non-local traffic to the IGW.

![NAT](pics/nat gateway.png)

## Launch Private Server
We launch the private server instance (Ubuntu) in the private subnet.

![Instance in Subnet](pics/instance subnet.png)

## Bastion
We launch another instance in the public subnet, which we will use as bastion.

## Route Tables
We create two route tables, one each for the public and the private subnet.
The route table for the private Subnet must direct all non explicit traffic to 
the NAT instance. The public route table directs to the IGW.
![Route Public](pics/route_public.png)
![Route Private](pics/route_private.png)

Private Server instance does not have a public IP:
![No IP](pics/private server no ip.png)

## Security Groups
To make the private instance only accessable via the bastion, we need to 
create two security groups with different settings. The security group for the 
private instance must only allow traffic from the security group for the bastion.

![Security Groups](pics/sgs.png)

## Connect to private instance & ping
SSH connection to the private instance is enabled by copying the key file for the 
private instance with scp to the bastion and connecting from the bastion to 
the private instance.

Everything works -> we can ping!

![Ping](pics/ping.png)

